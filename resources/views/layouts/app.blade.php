<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ config('app.name', 'Laravel') }}</title>

	<!-- Styles -->
	<link href="{{ asset('css/app.css') }}" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Merriweather|PT+Sans" rel="stylesheet"> 
</head>
<body>
	<div id="app">
		<header>
			<section class="title">
				<a href="/"><img src="/img/patchpng.png" alt="Brockport Fire District Patch"></a>
				<h1>Brockport Fire District</h1>
				<h2>...serving Brockport, Clarkson &amp; Sweden</h2>
			</section>
			<navigation></navigation> <!-- /resources/assets/js/components/Navigation.vue -->
		</header>
		@yield('content')
	</div>
	<footer>
		<div>
			<h5>Contact Us</h5>
			<ul>
				<li>
					Mike LaDue<br>
					Fire Chief (2C13)<br>
					<a href="mailto:firechief@brockport.org">firechief@brockportfire.org</a>
				</li>
				<li>
					Scott Smith<br>
					Deputy Chief (2C23)<br>
					<a href="mailto:deputychief@brockportfire.org">deputychief@brockportfire.org</a>
				</li>
				<li>
					Mike Menear<br>
					Assistant Chief (2C33)<br>
					<a href="mailto:assistantchief@brockportfire.org">assistantchief@brockportfire.org</a>
				</li>
				<li>
					Webmaster<br>
					<a href="mailto:webmaster@brockportfire.org">webmaster@brockportfire.org</a>
				</li>
			</ul>
		</div>
		<div>
			<h5>Quick Links</h5>
			<ul>
				<li><a href="#">Home</a></li>
				<li><a href="#">Events</a></li>
				<li><a href="#">About Us</a></li>
				<li><a href="#">Calendars</a></li>
				<li><a href="#">Contacts</a></li>
				<li><a href="#">Forms</a></li>
				<li><a href="#">Members Only</a></li>
			</ul>
		</div>
		<div>
			<h5>Location</h5>
			<p>
				Brockport Fire District<br>
				Station 3, 191 West Avenue<br>
				Brockport, NY<br>
				14420<br>
			</p>
			<p>
				Brockport Fire District<br>
				P.O. Box 131<br>
				Brockport<br>
				New York 14420-0131
			</p>
		</div>
	</footer>

	<!-- Scripts -->
	<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>