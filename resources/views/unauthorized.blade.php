@extends('layouts.app')

@section('content')
<main>
	<h6>You do not have permission to access this part of the site.</h6>
	<p>Please contact an administrator for further assistance.</p>
</main>

@endsection