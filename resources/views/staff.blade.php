@extends('layouts.app')

@section('content')
<main>
	<div class="list-label">
		<div class="expand-button">
			<div class="line-vertical"></div>
			<div class="line-horizontal"></div>
		</div>
		<h5>Fire District Commissioners</h5>
	</div>
	<ul>
		<li>
			<h6>Commissioner &amp; Chairman</h6>
			<p>Charles Sanford</p>
			<p><a href="mailto:csanford@brockportfire.org">csanford@brockportfire.org</a></p>
		</li>
		<li>
			<h6>Commissioner &amp; Vice-Chairman</h6>
			<p>Laurence Vaughan</p>
			<p><a href="mailto:lvaughann@brockportfire.org">lvaughann@brockportfire.org</a></p>
		</li>
		<li>
			<h6>Commissioner &amp; Secretary</h6>
			<p>Debra Bax</p>
			<p><a href="mailto:dbax@brockportfire.org">dbax@brockportfire.org</a></p>
		</li>
		<li>
			<h6>Commissioner</h6>
			<p>Willard Bird</p>
			<p><a href="mailto:bbird@brockportfire.org">bbird@brockportfire.org</a></p>
		</li>
		<li>
			<h6>Commissioner</h6>
			<p>James Sauberan</p>
			<p><a href="mailto:jsauberan@brockportfire.org">jsauberan@brockportfire.org</a></p>
		</li>
		<li>
			<h6>Treasurer</h6>
			<p>Harold Mundy</p>
			<p><a href="hmundy@brockportfire.org">hmundy@brockportfire.org</a></p>
		</li>
		<li>
			<h6>Attorney</h6>
			<p>Ray Diraddo</p>
			<p><a href="diraddolaw@gmail.com">diraddolaw@gmail.com</a></p>
		</li>
	</ul>
	<div class="list-label">
		<div class="expand-button">
			<div class="line-vertical"></div>
			<div class="line-horizontal"></div>
		</div>
		<h5>Chiefs &amp; Officers</h5>
	</div>
	<ul>
		<li>
			<h6>Fire Chief</h6>
			<p>Mike LaDue (2C13)</p>
		</li>
		<li>
			<h6>Deputy Chief</h6>
			<p>Scott Smith (2C23)</p>
		</li>
		<li>
			<h6>Assistant Chief</h6>
			<p>Michael Menear (2C33)</p>
		</li>
		<li>
			<h6>Fire Captain</h6>
			<p>Tim Russell (2C43)</p>
		</li>
		<li>
			<h6>Fire Captain</h6>
			<p>Scott Wainwright (2C53)</p>
		</li>
	</ul>

	<div class="list-label">
		<div class="expand-button">
			<div class="line-vertical"></div>
			<div class="line-horizontal"></div>
		</div>
		<h5>Board of Directors</h5>
	</div>
	<ul>
		<li>
			<h6>President</h6>
			<p>Michael Corey</p>
			<p><a href="mailto:president@brockportfire.org">president@brockportfire.org</a></p>
		</li>
		<li>
			<h6>Vice President</h6>
			<p>Larry Siegfried</p>
			<p><a href="vicepresident@brockportfire.org">vicepresident@brockportfire.org</a></p>
		</li>
		<li>
			<h6>Director</h6>
			<p>Dean Westcott</p>
			<p><a href="bod1@brockportfire.org">bod1@brockportfire.org</a></p>
		</li>
		<li>
			<h6>Director</h6>
			<p>Tim Henry</p>
			<p><a href="bod2@brockportfire.org">bod2@brockportfire.org</a></p>
		</li>
	</ul>
</main>

@endsection