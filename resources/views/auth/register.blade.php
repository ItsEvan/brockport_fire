@extends('layouts.app')

@section('content')
<main>
    <div class="center">
    <h5>Register Form</h5>
    </div>

    <form method="POST" action="{{ route('register') }}">
        {{ csrf_field() }}

        <div class="control{{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="name">Name</label>

            <input id="name" type="text" name="name" value="{{ old('name') }}" required>

            @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
            @endif
        </div>

        <div class="control{{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="email">E-Mail Address</label>

            <input id="email" type="email" name="email" value="{{ old('email') }}" required>

            @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
            @endif
        </div>

        <div class="control{{ $errors->has('password') ? ' has-error' : '' }}">
            <label for="password">Password</label>

            <input id="password" type="password" name="password" required>

            @if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
            @endif
        </div>

        <div class="control">
            <label for="password-confirm">Confirm Password</label>

            <input id="password-confirm" type="password" name="password_confirmation" required>
        </div>

        <div class="control">
            <button type="reset" class="secondary">Reset</button>
            <button type="submit" class="primary">Register</button>
        </div>
    </form>
</main>
@endsection
