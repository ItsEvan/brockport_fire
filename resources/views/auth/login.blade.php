@extends('layouts.app')

@section('content')
<div class="center"><h5>Login Form</h5></div>
<form method="POST" action="{{ route('login') }}">
    {{ csrf_field() }}

    <div class="control{{ $errors->has('email') ? ' has-error' : '' }}">
        <label for="email">E-Mail Address</label>

        <input id="email" type="email" name="email" value="{{ old('email') }}" required>

        @if ($errors->has('email'))
        <span class="help-block">
            <strong>{{ $errors->first('email') }}</strong>
        </span>
        @endif
    </div>

    <div class="control{{ $errors->has('password') ? ' has-error' : '' }}">
        <label for="password">Password</label>

        <input id="password" type="password" name="password" required>

        @if ($errors->has('password'))
        <span class="help-block">
            <strong>{{ $errors->first('password') }}</strong>
        </span>
        @endif
    </div>

    <div class="control">
        <button type="submit" class="primary">
            Login
        </button>
    </div>
    <div class="control">
        <div class="remember-me">
            <label>
                <input type="checkbox" class="center" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
            </label>
        </div>
    </div>
    <div class="control">
        <a href="{{ route('password.request') }}">
            Forgot Your Password?
        </a>
    </div>


</form>
@endsection
