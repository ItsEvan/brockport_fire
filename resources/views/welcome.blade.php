@extends('layouts.app')
@section('content')

<div class="hero">
	<figure>
		<img class="" src="/img/parade.jpg" alt="fire brigade parade">
		<figcaption>
			<h3>Protecting the Community.</h3>
			<a href="/docs/membership_letter.pdf"><h3>Join Us</h3></a>
		</figcaption>
	</figure>
</div>
<section class="intro">
	<figure>
		<a href="events">
			<img src="/img/calendar.jpg" alt="calendar">
			<div class="band">
				<h4>Events</h4>
			</div>
		</a>
	</figure>
	<figure>
		<a href="about">
			<img src="/img/remember.jpg" alt="station 4 museum">
			<div class="band">
				<h4>About Us</h4>
			</div>
		</a>
	</figure>
	<figure>
		<a href="staff">
			<img src="/img/members.jpg" alt="fire station members lineup">
			<div class="band">
				<h4>Staff</h4>
			</div>
		</a>
	</figure>
</section>
<main>
	<blockquote>
		The purpose of the Brockport Fire District is to provide efficient, effective and financially responsible fire and emergency services
	</blockquote>
	<p>
		Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias, ex saepe. Iste dolor, temporibus ea? Numquam error aut deleniti animi laborum, tenetur blanditiis. Dolor iste est quam, quos sequi illo neque hic maxime repellendus placeat quasi id quae ratione omnis numquam voluptatem voluptate odit, nesciunt ipsam. Nihil eius, eos atque.
	</p>
</main>



@endsection
