<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Middleware\Volunteer;

class MembersController extends Controller
{
    public function __construct() {

    	$this->middleware('volunteer');

    }

    public function index() {

    	return view('members.index');

    }
}
