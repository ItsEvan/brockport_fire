<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('about', function() {
	return view('about');
});

Route::get('/events', function() {
	return view('events');
});

Route::get('/contact', function() {
	return view('contact');
});

Route::get('/staff', function() {
	return view('staff');
});

Route::get('/members', 'MembersController@index');

Route::get('/register', 'RegisterController@showRegistrationForm');

Route::get('/login', 'LoginController@showLoginForm');

Route::get('/unauthorized', function() {
	return view('unauthorized');
});

Route::get('/members/list', 'EmployeeController@show');

Route::get('/members/schedules', 'ScheduleController@show');

Route::get('/members/sop', 'StandardOperatingProcedureController@show');

Auth::routes();